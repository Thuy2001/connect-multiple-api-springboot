package com.Springboot.Springboot.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("api/v1/execute")
public class RestAPIController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    final String URL="http://10.36.126.97:8000/gw/internal/ins-pti-service/api/v1/execute";
    @PostMapping(value = "/API1")
    public ObjectNode postAPI1(@RequestHeader HttpHeaders headers,
                              @RequestBody ObjectNode request){
        HttpEntity<?> entity = new HttpEntity<>(request,headers);
        ObjectNode response=restTemplate.exchange(URL,HttpMethod.POST,entity,ObjectNode.class).getBody();
        return response;
    }
    @PostMapping(value = "/API2")
    public ObjectNode postAPI2(@RequestHeader HttpHeaders headers, @RequestBody ObjectNode request){
        JsonNode Node1=request.get("request1");
        JsonNode Node2=request.get("request2");
        JsonNode Node3=request.get("request3");
        HttpEntity<?> entity1 = new HttpEntity<>(Node1,headers);
        HttpEntity<?> entity2 = new HttpEntity<>(Node2,headers);
        HttpEntity<?> entity3 = new HttpEntity<>(Node3,headers);
        ObjectNode response1=restTemplate.exchange(URL,HttpMethod.POST,entity1,ObjectNode.class).getBody();
        ObjectNode response2=restTemplate.exchange(URL,HttpMethod.POST,entity2,ObjectNode.class).getBody();
        ObjectNode response3=restTemplate.exchange(URL,HttpMethod.POST,entity3,ObjectNode.class).getBody();
//        ObjectNode objectNode= objectMapper.createObjectNode();
        ObjectNode objectNode = new ObjectNode(objectMapper.getNodeFactory());
        objectNode.set("request1",response1);

        objectNode.set("request2",response2);
        objectNode.set("request3",response3);
        return objectNode;
    }

}
